//
//  dbManagerTest.swift
//  CDOnlineUITests
//
//  Created by Lorenzo Adreani on 28/01/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import Foundation
import XCTest

@testable import CDOnline

class dbManagerTests: XCTestCase {
    
    func testDBConnection() {
        let dbm = DBManager()
        XCTAssertTrue(dbm.tryConnection())
    }
    
    func testSetUpDB() {
        let dbm = DBManager()
        XCTAssertTrue(dbm.setUpDatabase() == 4)
    }
    
    func testCreateCategoryTable() {
        let dbm = DBManager()
        print(Queries.createCategoryTable)
        XCTAssertTrue(dbm.createTable(sql: Queries.createCategoryTable, tableName: "categorie"))
    }
    
    func testCreateUser() {
        let dbm = DBManager()
        if dbm.setUpDatabase() == 4 {
            let c1 = Contact(nome: "lorenzo")
            c1.lastname = "adreani"
            c1.address = "via qualcosa"
            c1.phone = 313923039
            
            XCTAssertTrue(dbm.insertContact(user: c1))
        }
    }
}
