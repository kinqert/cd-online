//
//  DashboardViewController.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 04/02/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    var drls = [DataReloadable]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dr = segue.destination as? DataReloadable {
            drls.append(dr)
        }
    }
}
