//
//  DashLastTransactionTableViewController.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 10/02/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit

class DashLastTransactionTableViewController: UITableViewController {

    var transactions = [Transaction]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.clipsToBounds = true
        self.view.layer.cornerRadius = 20

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    private func loadData() {
        transactions = TransactionTable.lastTransaction(limit: 5)
        tableView.reloadData()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return transactions.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let transactionCellId = "TransactionViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: transactionCellId, for: indexPath) as? DashTransactionTableViewCell else {
            fatalError("Errore transaction cell")
        }

        let ts = transactions[indexPath.row]
        let contact = ContactTable.getFromId(id: ts.contact)
        cell.nameLbl.text = "\(contact?.name ?? "") \(contact?.lastname ?? "")"
        cell.img.image = contact?.image
        cell.amountLbl.text = "Amount: \(ts.amount)$"
        cell.amountLbl.textColor = ts.recived ? Colors.greenDark : Colors.redDark
        cell.dateLbl.text = ts.date
        
        return cell
    }

    override func viewDidAppear(_ animated: Bool) {
        loadData()
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
