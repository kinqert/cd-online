//
//  NewActionViewController.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 25/01/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit

protocol NewActionDelegate{
    func actionDone(newActionViewController: NewActionViewController)
}

class NewActionViewController: UIViewController {
    
    @IBOutlet weak var actionView: UIView!
    
    var actionVc: UIViewController?
    var delegate: NewActionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // self.startAvoidingKeyboard()
    }
    
    private func newTransaction() {
        actionVc?.viewDidLoad()
       
        self.actionView.alpha = 1
        self.actionView.center.y = self.view.center.y + self.view.bounds.height
        UIView.animate(withDuration: 0.5) {
            self.actionView.center.y -= self.view.bounds.height
        }
    }
    
    private func removeView() {
//        actionView?.removeFromSuperview()
//        actionView = nil
        
        performSegue(withIdentifier: "unwindSegueToDash", sender: self)
    }
    
    @IBAction func addTap(_ sender: Any) {
        let alert = UIAlertController(title: "New Action", message: "Select the new action to execute", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Add Transaction", style: .default, handler: { _ in
            // ** Nuova transazione **
            self.newTransaction()
        }))
        alert.addAction(UIAlertAction(title: "Add Debit", style: .default, handler: { _ in
            // ** Nuovo debito **
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            // ** Annula l'operazione **
        }))
        present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let ts = segue.destination as? TransactionViewController, segue.identifier == "transactionSegue" {
            actionVc = ts
            ts.delegate = self
            
            self.actionView.center.y += self.view.bounds.height
            UIView.animate(withDuration: 0.5) {
                self.actionView.center.y -= self.view.bounds.height
            }
        }
    }
}


extension NewActionViewController: TransactionViewControllerDelegate {
    func endedTransaction(transaction: TransactionViewController, saved: Bool) {
        if saved {
            UIView.animate(withDuration: 0.5, animations: {
                self.actionView.center.y -= self.view.bounds.height
            }) { (finished) in
                self.removeView()
            }
        } else {
            UIView.animate(withDuration: 0.25, animations: {
                self.actionView.alpha = 0
            }) { (finished) in
                self.removeView()
            }
        }
    }
}
