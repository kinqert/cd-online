//
//  Transaction.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 05/02/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import Foundation

func getTodayString() -> String{
    
    let date = Date()
    let calender = Calendar.current
    let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
    
    let year = components.year
    let month = components.month
    let day = components.day
    let hour = components.hour
    let minute = components.minute
    let second = components.second
    
    let today_string = String(year!) + "-" + String(month!) + "-" + String(day!) + " " + String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
    
    return today_string
    
}

class Transaction {
    public var id: Int64
    public var contact: Int64
    public var recived: Bool
    public var amount: Double
    public var date: String
    public var motivation: String?
    
    init() {
        self.id = -1
        self.contact = -1
        self.recived = false
        self.amount = 0
        self.date = ""
    }
    
    init(id: Int64 = -1, contact: Int64, recived: Bool, amount: Double) {
        self.id = id
        self.contact = contact
        self.recived = recived
        self.amount = amount
        self.date = getTodayString()
    }
}
