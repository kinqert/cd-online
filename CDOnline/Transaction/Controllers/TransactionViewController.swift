//
//  TransactionViewController.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 06/02/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit

protocol TransactionViewControllerDelegate {
    func endedTransaction(transaction: TransactionViewController, saved: Bool)
}

class TransactionViewController: UIViewController {
    var delegate: TransactionViewControllerDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var rsSegment: UISegmentedControl!
    @IBOutlet weak var contactSlider: UIView!
    
    
    var sliderController: ContactSlider?
    let formatter = NumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.layer.borderWidth = 1
        self.view.layer.borderColor = UIColor.white.cgColor
        self.view.clipsToBounds = true
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.barStyle = .black
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.amountDone))
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: #selector(self.amountDone))
        toolbar.setItems([space, doneBtn], animated: false)
        
        amountField.inputAccessoryView = toolbar
        amountField.layer.cornerRadius = 10
        amountField.layer.borderWidth = 1
        amountField.layer.borderColor = UIColor.white.cgColor
        amountField.delegate = self
        
        newTransaction()
    }
    
    func newTransaction() {
        titleLabel.text = "New transaction"
        rsSegment.selectedSegmentIndex = 0
        saveBtn.isEnabled = false
        amountField.text = formatter.string(from: NSNumber.init(value: 0.0))
        changeControlColors(color: Colors.greenDark)
    }
    
    @IBAction func cdChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            changeControlColors(color: Colors.greenDark)
        } else {
            changeControlColors(color: Colors.redDark)
        }
    }
    
    @objc func amountDone() {
        self.view.endEditing(true)
        validateTransaction()
    }
    
    
    // MARK: - Private section
    private func changeControlColors(color: UIColor) {
        rsSegment.tintColor = color
        saveBtn.tintColor = color
        amountField.textColor = color
    }
    
    private func createTransactions() -> [Transaction] {
        var ts = [Transaction]()
        
        for c in sliderController?.selectedContact ?? [] {
            let amount = formatter.number(from: amountField.text!)
            let recived = rsSegment.selectedSegmentIndex == 0
            let t = Transaction(contact: Int64(c.id), recived: recived, amount: amount!.doubleValue)
            ts.append(t)
        }
        
        return ts
    }
    
    @IBAction func saveTap(_ sender: Any) {
        var ris = true
        for t in createTransactions() {
            ris = ris && TransactionTable.insert(transaction: t)
        }
        
        if !ris {
            NSLog("Errore durante il salvataggio di qualche transizione")
        }
        
        self.delegate?.endedTransaction(transaction: self, saved: true)
    }
    
    @IBAction func closeTap(_ sender: Any) {
        self.delegate?.endedTransaction(transaction: self, saved: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cs = segue.destination as? ContactSlider, segue.identifier == "sliderSegue" {
            sliderController = cs
            sliderController?.delegate = self
        }
    }
    
    private func validateTransaction() {
        if sliderController?.nrSelected == 0 || formatter.number(from: amountField.text!) == 0 {
            saveBtn.isEnabled = false
        } else {
            saveBtn.isEnabled = true
        }
    }
}

extension TransactionViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let finalString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        if let nr = formatter.number(from: finalString + "0") {
            if nr == formatter.number(from: textField.text!) && string != "" && string != "," && string != "." {
                return false
            }
            return true
        }
        return false
    }
}

extension TransactionViewController: ContactSliderViewControllerDelegate {
    func contactsSelectedChange(contactSliderViewController: ContactSlider) {
        titleLabel.text = "\(contactSliderViewController.nrSelected) new transaction"
        validateTransaction()
    }
}
