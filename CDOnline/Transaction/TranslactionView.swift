//
//  TranslactionView.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 29/01/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit

@IBDesignable class TransactionView: UIRoundedView {

    @IBOutlet var view: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("TranslactionView", owner: self, options: nil)
        self.addSubview(self.view)
    }

}
