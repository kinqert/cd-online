//
//  TabController.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 26/01/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit
import os.log

class TabController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        delegate = self
    }
    
//    // MARK: - UITabBarControllerDelegate
//    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
//        guard let nv = viewController as? UINavigationController else { return }
//        guard let dr = nv.visibleViewController as? DataReloadable  else { return }
//
//        dr.reload()
//    }
    
    @IBAction func unwindToDashboard(segue: UIStoryboardSegue) {
        selectedIndex = 0
    }
}
