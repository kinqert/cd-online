//
//  DataReloadable.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 14/02/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import Foundation

protocol DataReloadable {
    func reload()
}
