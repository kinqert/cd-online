//
//  Observer.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 05/02/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import Foundation


protocol Observer {
    func update()
}

protocol Target {
    var Obs: [Observer]{get set}
    func addObs(o: Observer)
    func removeObs(at: Int)
    func notify()
}
