//
//  TransactionTable.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 05/02/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import Foundation
import SQLite

class TransactionTable {
    static let transactions = Table("transazioni")
    static let id = Expression<Int64>("id")
    static let contact = Expression<Int64>("contact")
    static let recived = Expression<Bool>("recived")
    static let importo = Expression<Double>("importo")
    static let data = Expression<String>("data")
    static let motivation = Expression<String?>("motivation")
    
    
    static func createTableOnDB() -> Bool {
        return DBManager.instance.executeOnDBSafe(execute: { (db) in
            try db.run(transactions.create(ifNotExists: true) { t in
                t.column(id, primaryKey: true)
                t.column(contact, references: ContactTable.contacts, ContactTable.id)
                t.column(recived)
                t.column(importo)
                t.column(data)
                t.column(motivation)
            })
        }, sString: "Tabella transazioni operativa", eString: "Errore durante la creazione della tabella")
    }
    
    static func insert(transaction: Transaction) -> Bool {
        let insert = transactions.insert(
            contact <- transaction.contact,
            recived <- transaction.recived,
            importo <- transaction.amount,
            data <- transaction.date,
            motivation <- transaction.motivation
        )
        
        return DBManager.instance.executeOnDBSafe(execute: { (db) in
            transaction.id = try db.run(insert)
        }, sString: "Transazione inserita id: \(transaction.id)", eString: "Errore durante l'inserimento della transizione")
    }
    
    static private func tFromRow(r: Row) -> Transaction {
        let t = Transaction(id: r[id], contact: r[contact], recived: r[recived], amount: r[importo])
        t.date = r[data]
        t.motivation = r[motivation]
        return t
    }
    
    static func getAll() -> [Transaction] {
        var tlist = [Transaction]()
        
        _ = DBManager.instance.executeOnDBSafe(execute: { (db) in
            for r in try db.prepare(transactions) {
                tlist.append(tFromRow(r: r))
            }
        }, sString: "\(tlist.count) contatti scaricati", eString: "Errore durante il ricevimento di uno dei contatti")

        return tlist
    }
    
    static func lastTransaction(limit: Int) -> [Transaction] {
        var tlist = [Transaction]()
        
        _ = DBManager.instance.executeOnDBSafe(execute: { (db) in
            for r in try db.prepare(transactions.order(id.desc).limit(limit)) {
                tlist.append(tFromRow(r: r))
            }
        }, sString: "\(tlist.count) contatti scaricati", eString: "Errore durante il ricevimento di uno dei contatti")

        return tlist
    }
    
    static func update(transaction: Transaction) -> Bool{
        let t = transactions.filter(id == transaction.id)
        let update = t.update(
            contact <- transaction.contact,
            recived <- transaction.recived,
            importo <- transaction.amount,
            data <- transaction.date,
            motivation <- transaction.motivation)
        
        return DBManager.instance.executeOnDBSafe(execute: { (db) in
            try db.run(update)
        }, sString: "transizione \(transaction.id) aggiornata", eString: "Errore durante l'aggiornamento della transizione: \(transaction.id)")
    }
    
    static func delete(transaction: Transaction) -> Bool {
        let t = transactions.filter(id == transaction.id)
        
        return DBManager.instance.executeOnDBSafe(execute: { (db) in
            try db.run(t.delete())
        }, sString: "transizione \(transaction.id) cancellata", eString: "Errore durante la cancellazione della transizione: \(transaction.id)")
    }
}
