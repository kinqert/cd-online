//
//  DBManager.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 27/01/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import Foundation
import SQLite3
import UIKit
import SQLite

class DBManager: NSObject {
    public static let instance: DBManager = DBManager()
    var path: String?
    var db: Connection?
    
    private override init() {
        super.init()
        self.path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    }
    
    public func openConnection() -> Bool {
        if self.path == nil {
            NSLog("Path database vuota")
            return false
        }
        
        do {
            db = try Connection("\(self.path!)/db.sqlite3")
        } catch {
            NSLog("errore durante la connessione al database")
            return false
        }
        NSLog("connessione al database aperta")
        return true
    }
    
    public func executeOnDBSafe(execute: (Connection) throws -> Void, sString: String? = nil, eString: String? = nil) -> Bool {
        if openConnection() {
            do {
                try execute(db!)
                NSLog(sString ?? "")
                return true
            } catch {
                NSLog(eString ?? "")
            }
        }
        return false
    }
    
    // MARK: - create table methods
    public func setUpDatabase() -> Bool{
        var ris = true
        ris = ris && openConnection()
        ris = ris && ContactTable.createTableOnDB()
        ris = ris && TransactionTable.createTableOnDB()
        return ris
    }
}
