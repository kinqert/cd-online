//
//  userTable.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 29/01/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import Foundation
import SQLite

public class ContactTable {
    static let contacts = Table("Contact")
    static let id = Expression<Int64>("idContatti")
    static let name = Expression<String>("name")
    static let lastname = Expression<String?>("lastname")
    static let address = Expression<String?>("address")
    static let email = Expression<String?>("email")
    static let phone = Expression<String?>("phone")
    static let note = Expression<String?>("note")
    static let image = Expression<String?>("image")
    
    private init() {}
    
    static func createTableOnDB() -> Bool {
        return DBManager.instance.executeOnDBSafe(execute: { (db) in
            try db.run(contacts.create(ifNotExists: true) { t in
                t.column(id, primaryKey: true)
                t.column(name)
                t.column(lastname)
                t.column(address)
                t.column(email)
                t.column(phone)
                t.column(note)
                t.column(image)
            })
        }, sString: "Tabella contatti funzionante", eString: "Errore durante la creazione della tabbella contatti")
    }
    
    static func insert(contact: Contact) -> Bool {
        let insert = contacts.insert(
            name <- contact.name,
            lastname <- contact.lastname,
            address <- contact.address,
            email <- contact.email,
            phone <- contact.phone,
            note <- contact.note,
            image <- contact.getTextImage()
        )
        
        return DBManager.instance.executeOnDBSafe(execute: { (db) in
            contact.id = try Int(db.run(insert))
        }, sString: "Inserimento effettuato id: \(contact.id)", eString: "Errore durante l inserimento del contatto")
    }
    
    static private func cFromRow(r: Row) -> Contact {
        let c = Contact(name: r[name], id: Int(r[id]))
        c.lastname = r[lastname]
        c.address = r[address]
        c.email = r[email]
        c.phone = r[phone]
        c.note = r[note]
        c.setImageFromText(img: r[image] ?? "")
        
        return c
    }
    
    static func getFromId(id: Int64) -> Contact? {
        var c = Contact()
        _ = DBManager.instance.executeOnDBSafe(execute: { (db) in
            for r in try db.prepare(contacts.filter(self.id == id)) {
                c = cFromRow(r: r)
            }
        }, sString: "Contatto \(id) caricato", eString: "Errore caricamento contatto \(id)")
        
        return c.id == -1 ? nil : c
    }
    
    static func getAll() -> [Contact] {
        var clist = [Contact]()
        
        _ = DBManager.instance.executeOnDBSafe(execute: { (db) in
            for r in try db.prepare(contacts) {
                clist.append(cFromRow(r: r))
            }
        }, sString: "Lista contatti caricata", eString: "Errore durante il caricamento dei contatti")
        
        return clist
    }
    
    static func update(contact: Contact) -> Bool{
        let c = contacts.filter(id == Int64(contact.id))
        let update = c.update(
            name <- contact.name,
            lastname <- contact.lastname,
            address <- contact.address,
            email <- contact.email,
            phone <- contact.phone,
            note <- contact.note,
            image <- contact.getTextImage())
        
        return DBManager.instance.executeOnDBSafe(execute: { (db) in
            try db.run(update)
        }, sString: "contatto \(contact.id) aggiornato", eString: "Errore durante l'aggiornamento del contatto \(contact.id)")
    }
    
    static func delete(contact: Contact) -> Bool {
        return DBManager.instance.executeOnDBSafe(execute: { (db) in
            try db.run(contacts.filter(id == Int64(contact.id)).delete())
        }, sString: "contatto \(contact.id) cancellato", eString: "Errore durante la cancellazione del contatto \(contact.id)")
    }
}
