//
//  ImageCircularView.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 25/01/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit

@IBDesignable class ImageCircularView: UIImageView {
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    // MARK: Inizializzazione
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
