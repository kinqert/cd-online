//
//  Colors.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 05/02/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit

class Colors {
    static let greenDark = UIColor(red: 71 / 255, green: 191 / 255, blue: 103 / 255, alpha: 1)
    static let redDark = UIColor(red: 191 / 255, green: 53 / 255, blue: 53 / 255, alpha: 1)
    static let blue = UIColor(red: 66 / 255, green: 134 / 255, blue: 244 / 255, alpha: 1)
}
