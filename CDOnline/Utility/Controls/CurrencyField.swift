//
//  CurrencyField.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 15/02/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit

class CurrencyField: UITextField, UITextFieldDelegate {

    let formatter = NumberFormatter()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.keyboardType = .decimalPad
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        self.text = formatter.string(from: NSNumber.init(value: 0.0))
    }
    
    func getCurrencyNumber() -> Double {
        return Double(truncating: formatter.number(from: text!)!)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let finalString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        if let nr = formatter.number(from: finalString + "0") {
            if nr == formatter.number(from: textField.text!) && string != "" && string != "," && string != "." {
                return false
            }
            return true
        }
        return false
    }
    
}
