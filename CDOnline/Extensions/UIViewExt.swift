//
//  UIViewExt.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 06/02/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    internal func loadFirstViewNib(nibName: String) {
        if let view = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?.first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
    }
}
