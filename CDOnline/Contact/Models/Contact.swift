//
//  User.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 24/01/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit
import os.log

class Contact {
    // MARK: - Properties
    
    var id: Int
    var name: String
    var lastname: String?
    var address: String?
    var email: String?
    var phone: String?
    var note: String?
    
    var image: UIImage?
    
    
    // MARK: - Init
    init() {
        self.name = ""
        self.id = -1
    }
    
    init(name: String, id: Int = -1) {
        self.name = name
        self.id = id
    }
    
    func setImageFromText(img: String) {
        let data = Data(base64Encoded: img, options: .ignoreUnknownCharacters)
        self.image = data != nil ? UIImage(data: data!) : UIImage(named: "defaultUser")
    }
    
    func getTextImage() -> String {
        var imageTxt = ""
        if image != nil {
            let data: Data? = image!.pngData()
            imageTxt = (data?.base64EncodedString(options: .endLineWithLineFeed))!
        }
        return imageTxt
    }
    
}
