//
//  ContactMiniViewController.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 06/02/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit

protocol ContactMiniViewControllerDelegate {
    func contactSelected(contactMini: ContactMini)
}

class ContactMini: UIView {
    public var delegate: ContactMiniViewControllerDelegate?
    public var isSelectable = true
    public var selected = false {
        didSet {
            if selected {
                viewRt.layer.borderWidth = 2
            } else {
                viewRt.layer.borderWidth = 0
            }
        }
    }
    
    var contact: Contact? {
        didSet {
            reloadContactInfo()
        }
    }
    
    @IBOutlet weak var viewRt: UIView!
    @IBOutlet weak var imgCircView: ImageCircularView!
    @IBOutlet weak var nameLbl: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        loadFirstViewNib(nibName: "ContactMini")
        
        viewRt.layer.cornerRadius = 10
        viewRt.clipsToBounds = true
        viewRt.layer.borderColor = Colors.blue.cgColor
        
        imgCircView.cornerRadius = imgCircView.frame.height / 2
    }
    
    // MARK: - Private
    func reloadContactInfo() {
        if let c = contact {
            imgCircView.image = c.image
            nameLbl.text = "\(c.name) \(c.lastname ?? "")"
        }
    }
    
    
    @IBAction func tapped(_ sender: UITapGestureRecognizer) {
        selectOrDeselect()
    }
    
    func selectOrDeselect() {
        if isSelectable {
            selected = !selected
            delegate?.contactSelected(contactMini: self)
        }
    }
}
