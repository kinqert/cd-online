//
//  UserTableViewController.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 24/01/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit
import os.log

class ContactTableViewController: UITableViewController {
    // MARK: Properties
    var contacts = [Contact]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.leftBarButtonItem = self.editButtonItem
        
        contacts += ContactTable.getAll()
    }
    
    // MARK: - Actions
    @IBAction func unwindToUserList(sender: UIStoryboardSegue) {
        if let srcViewCont = sender.source as? ContactViewController, let contact = srcViewCont.contact {
            if let selectedIndex = tableView.indexPathForSelectedRow {
                // Modifica utente esistente
                if ContactTable.update(contact: contact) {
                    contacts[selectedIndex.row] = contact
                    tableView.reloadRows(at: [selectedIndex], with: .none)
                    NSLog("Elemento aggiornato")
                }
            } else {
                // Aggiunta di un nuovo utente
                let newIndexPath = IndexPath(row: contacts.count, section: 0)
                if ContactTable.insert(contact: contact) {
                    contacts.append(contact)
                    tableView.insertRows(at: [newIndexPath], with: .automatic)
                    NSLog("Elemento aggiunto")
                }
            }
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "UserTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? ContactTableViewCell
            else {
                fatalError("Errore contact table cell")
        }
        let user = contacts[indexPath.row]
        cell.nameLabel.text = "\(user.name) \(user.lastname ?? "")"
        cell.photoView.image = user.image

        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // cancellazione
            if ContactTable.delete(contact: contacts[indexPath.row]) {
                contacts.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier ?? "" {
            
        case "AddItem":
            os_log("Adding a new user", log: OSLog.default, type: .debug)
            
        case "ShowDetail":
            guard let userDetailVC = segue.destination as? ContactViewController else {
                fatalError("Errore: Non e' un UserViewController")
            }
            guard let selectedUserCell = sender as? ContactTableViewCell else {
                fatalError("Errore: La cella selezionata non e' UserTableViewCell")
            }
            guard let indexPath = tableView.indexPath(for: selectedUserCell) else {
                fatalError("La cella non e' stata selezionata da questa tabella")
            }
            
            let selectedUser = contacts[indexPath.row]
            userDetailVC.contact = selectedUser
            
        default:
            fatalError("Errore: Segue non identificato")
        }
    }
}
