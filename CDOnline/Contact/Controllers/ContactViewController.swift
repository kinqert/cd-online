//
//  UserViewController.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 25/01/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit
import os.log

class ContactViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    var isInAddMode = false
    var contact: Contact?
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var numberField: UITextField!
    @IBOutlet weak var addressField: UITextView!
    @IBOutlet weak var noteField: UITextView!
    
    @IBOutlet weak var imageCircularView: ImageCircularView!
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.startAvoidingKeyboard()

        nameField.delegate = self
        lastNameField.delegate = self
        emailField.delegate = self
        numberField.delegate = self
        
        // TODO add text view delegate
        // addressField.delegate = self
        // noteField.delegate = self
        
        for sb in self.view.subviews {
            if let tf = sb as? UITextField {
                tf.clipsToBounds = true
                tf.layer.borderWidth = 1
                tf.layer.cornerRadius = 20
                tf.layer.borderColor = UIColor.white.cgColor
            }
        }
        
        if let user = contact {
            navigationItem.title = user.name
            
            nameField.text = user.name
            lastNameField.text = user.lastname
            emailField.text = user.email
            numberField.text = user.phone
            addressField.text = user.address
            noteField.text = user.note
            
            imageCircularView.image = user.image
            isInAddMode = false
        } else {
            isInAddMode = true
        }
        
        updateButtonSave()
    }
    
    // MARK: - Private Methods
    private func setPlaceHolder() {
        let color = UIColor.white
        
        // TO FINISH
        
        nameField.attributedPlaceholder = NSAttributedString(string: "insert name...", attributes: [NSAttributedString.Key.foregroundColor: color])
        lastNameField.attributedPlaceholder = NSAttributedString(string: "inserisci nome...", attributes: [NSAttributedString.Key.foregroundColor: color])
        emailField.attributedPlaceholder = NSAttributedString(string: "inserisci nome...", attributes: [NSAttributedString.Key.foregroundColor: color])
    }
    
    private func updateButtonSave() {
        let text = nameField.text ?? ""
        saveButton.isEnabled = !text.isEmpty
    }

    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        NSLog("Image tapped")
        selectPhoto()
    }
    
    func selectPhoto() {
        NSLog("Image tapped")
        nameField.resignFirstResponder()
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        saveButton.isEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateButtonSave()
        navigationItem.title = nameField.text
    }
    
    // MARK: - ImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedPhoto = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            fatalError("Errore: \(info)")
        }
        imageCircularView.image = selectedPhoto
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        if isInAddMode {
            dismiss(animated: true, completion: nil)
        } else if let modViewCont = navigationController {
            modViewCont.popViewController(animated: true)
        } else {
            fatalError("UserController non è in alcun navigationController")
        }
        
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            os_log("Il bottone salva non e' stato premuto... cancellazzione", log: OSLog.default, type: .debug)
            return
        }
        
        if nameField.text == nil {
            return
        }
        
        if isInAddMode {
            contact = Contact(name: nameField.text!)
        } else {
            contact?.name = nameField.text!
        }
        
        
        contact?.lastname = lastNameField.text
        contact?.address = addressField.text
        contact?.email = emailField.text
        contact?.phone = numberField.text
        contact?.note = noteField.text
        contact?.image = imageCircularView.image
    }

}
