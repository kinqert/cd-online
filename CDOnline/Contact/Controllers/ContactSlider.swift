//
//  ContactSliderViewController.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 06/02/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit

protocol ContactSliderViewControllerDelegate {
    func contactsSelectedChange(contactSliderViewController: ContactSlider)
}

class ContactSlider: UIViewController {
    private var contactsMini = [ContactMini]()
    public var nrSelected = 0
    public var selectedContact = [Contact]()
    public var delegate: ContactSliderViewControllerDelegate?
    
    @IBOutlet weak var contactMiniStack: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpContactsMini()
    }
    
    private func clear() {
        contactsMini = [ContactMini]()
        nrSelected = 0
        selectedContact = [Contact]()
        for v in contactMiniStack.arrangedSubviews {
            v.removeFromSuperview()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setUpContactsMini()
    }
    
    func setUpContactsMini() {
        clear()
        
        for c in ContactTable.getAll() {
            let cmini = ContactMini()
            cmini.delegate = self
            
            contactsMini.append(cmini)
            contactMiniStack?.addArrangedSubview(cmini)
            
            cmini.contact = c
            cmini.translatesAutoresizingMaskIntoConstraints = false
            
            NSLayoutConstraint(item: cmini, attribute: .height, relatedBy: .equal, toItem: contactMiniStack, attribute: .height, multiplier: 1, constant: 0).isActive = true
            NSLayoutConstraint(item: cmini, attribute: .width, relatedBy: .equal, toItem: contactMiniStack, attribute: .height, multiplier: 1, constant: 0).isActive = true
        }
    }
    
    public func getSelectedContact() -> [Contact] {
        var cs = [Contact]()
        for c in contactsMini {
            if c.selected && c.contact != nil {
                cs.append(c.contact!)
            }
        }
        return cs
    }
}

extension ContactSlider: ContactMiniViewControllerDelegate {
    func contactSelected(contactMini: ContactMini) {
        if contactMini.contact == nil {
            return
        }
        
        if contactMini.selected {
            nrSelected += 1
            selectedContact.append(contactMini.contact!)
        } else {
            nrSelected -= 1
            selectedContact = selectedContact.filter({ (c) -> Bool in
                return c.id != contactMini.contact?.id
            })
        }
        
        delegate?.contactsSelectedChange(contactSliderViewController: self)
    }
}
