//
//  UserTableViewCell.swift
//  CDOnline
//
//  Created by Lorenzo Adreani on 24/01/2019.
//  Copyright © 2019 Lorenzo Adreani. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var photoView: ImageCircularView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
